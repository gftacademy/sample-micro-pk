package pl.gftacademy.micro.sample;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.RoutingHandler;
import io.undertow.util.Methods;

public class SampleMicro {

    public static void main(String[] args) {
        RoutingHandler routingHandler = Handlers.routing()
                .add(Methods.GET, "/health", new HealthHandler());
        Undertow server = Undertow.builder()
                .addHttpListener(8127, "0.0.0.0")
                .setHandler(routingHandler)
                .build();
        server.start();
    }

}
