package pl.gftacademy.micro.sample;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

class HealthHandler implements HttpHandler {
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        JsonObject jsonElement = new JsonObject();
        jsonElement.addProperty("component", System.getProperty("app.component"));
        jsonElement.addProperty("version", HealthHandler.class.getPackage().getImplementationVersion());


        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
        Gson gson = new GsonBuilder().setPrettyPrinting()
                .create();
        String response = gson.toJson(jsonElement);
        exchange.getResponseSender().send(response);
    }
}
